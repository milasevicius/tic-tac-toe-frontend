import React from 'react'
import { createStore, applyMiddleware } from 'redux'
import { Provider } from 'react-redux'
import thunkMiddleware from 'redux-thunk'

import reducers from './reducers'
import { loggingMiddleware } from './actions/log'

import Game from './containers/Game'
import Log from './containers/Log'

const store = createStore(
  reducers,
  applyMiddleware(loggingMiddleware, thunkMiddleware)
)

const App = () => (
  <Provider store={store}>
    <div>
      <Game />
      <Log />
    </div>
  </Provider>
)

export default App
