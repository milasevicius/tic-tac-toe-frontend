const INITIAL_STATE = {
  data: [null, null, null, null, null, null, null, null, null],
  turnIndex: 0,
  hasMoves: true,
  hasWon: false
}

function hasMoves(data) {
  return data.indexOf(null) > -1
}

function hasWon(data) {
  const winningCombos = [
    // Horizontal
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    // Vertical
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    // Diagonal
    [0, 4, 8],
    [2, 4, 6]
  ]

  const hasWon = winningCombos
    .map(combo => (
      data[combo[0]] &&
      data[combo[0]] === data[combo[1]] &&
      data[combo[0]] === data[combo[2]]
    ))
    .indexOf(true) > -1

  return hasWon
}

const game = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case 'RESTART':
      return INITIAL_STATE

    case 'FILL':
    // If cell is filled or someone won, do not let user to fill it again
    if (state.data[action.index] || hasWon(state.data)) {
      return state
    }

    // Lets recalc new data
    const data = [
      ...state.data.slice(0, action.index),
      state.turnIndex % 2 === 0 ? 'X' : 'O',
      ...state.data.slice(action.index + 1)
    ]

      return {
        ...state,
        data,
        turnIndex: state.turnIndex + 1,
        hasMoves: hasMoves(data),
        hasWon: hasWon(data)
      }

    default:
      return state
  }
}

export default game
