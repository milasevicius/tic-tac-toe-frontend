import { combineReducers } from 'redux'

import game from './game'
import log from './log'

export default combineReducers({
  game,
  log
})
