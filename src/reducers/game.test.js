import reducer from './game'

describe('Game reducer', () => {
  const initialState = {
    data: [ null, null, null, null, null, null, null, null, null ],
    turnIndex: 0,
    hasMoves: true,
    hasWon: false
  }

  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual(initialState)
  })

  it('should fill selected cell', () => {
    expect(
      reducer(undefined, {
        type: 'FILL',
        index: 5
      })
    ).toEqual({
      data: [ null, null, null, null, null, "X", null, null, null ],
      hasMoves: true,
      hasWon: false,
      turnIndex: 1
    })
  })

  it('should compute if player won the game', () => {
    expect(
      reducer({
        data: [ 'X', 'X', null, 'O', 'O', null, null, null, null ],
        hasMoves: true,
        hasWon: false,
        turnIndex: 4
      }, {
        type: 'FILL',
        index: 2
      })
    ).toEqual({
      data: [ 'X', 'X', 'X', 'O', 'O', null, null, null, null ],
      hasMoves: true,
      hasWon: true,
      turnIndex: 5
    })
  })

  it('should restart game', () => {
    expect(
      reducer({
        data: [ 'X', 'X', 'X', 'O', 'O', null, null, null, null ],
        hasMoves: true,
        hasWon: true,
        turnIndex: 5
      }, {
        type: 'RESTART'
      })
    ).toEqual(initialState)
  })
})
