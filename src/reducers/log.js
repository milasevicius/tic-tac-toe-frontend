const INITIAL_STATE = {
  data: [],
  log: true
}

const log = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case 'TOGGLE_LOG':
      return {
        ...state,
        log: !state.log
      }

    case 'RECEIVE_LOG':
      return {
        ...state,
        data: action.data
      }

    default:
      return state
  }
}

export default log
