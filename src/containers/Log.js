import React, { Component } from 'react'
import { connect } from 'react-redux'

import { fetchLog } from '../actions/log'

import Container from '../components/Container'

class Log extends Component {
  componentDidMount() {
    this.props.fetchLog(true)
  }

  render() {
    const { data } = this.props.log

    return (
      <Container>
        <ul>
          {data.map((item, i) => (
            <li key={i}>
              <pre>
                {JSON.stringify(item)}
              </pre>
            </li>
          ))}
        </ul>
      </Container>
    )
  }
}

const mapStateToProps = state => ({
  log: state.log
})

const mapDispatchToProps = dispatch => ({
  fetchLog: initial => dispatch(fetchLog(initial))
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Log)
