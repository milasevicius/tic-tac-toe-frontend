import React from 'react'
import { connect } from 'react-redux'

import Container from '../components/Container'
import TTTGrid from '../components/TTTGrid'
import TTTCell from '../components/TTTCell'

import { restart, fill } from '../actions/game'

const Game = (props) => {
  const { data, turnIndex, hasMoves, hasWon } = props.game
  const { restart, fill } = props

  return (
    <Container>
      <p>
        {hasMoves && !hasWon ? (
          <span>It's <strong>{turnIndex % 2 === 0 ? 'X' : 'O'}</strong> turn.</span>
        ) : (
          <span>
            {hasWon ? (
              <span><strong>{turnIndex % 2 === 0 ? 'O' : 'X'}</strong> won the game. </span>
            ) : (
              <span>It's a tie. </span>
            )}
            <button onClick={restart}>Restart?</button>
          </span>
        )}
      </p>
      <TTTGrid>
        {data.map((cell, i) => (
          <TTTCell key={i} fill={cell} onClick={() => fill(i)}>{cell}</TTTCell>
        ))}
      </TTTGrid>
    </Container>
  )
}

const mapStateToProps = state => ({
  game: state.game
})

const mapDispatchToProps = dispatch => ({
  restart: () => dispatch(restart()),
  fill: i => dispatch(fill(i))
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Game)
