import styled from 'styled-components'

const TTTCell = styled.div`
  margin: 2px;
  width: calc(50px - 4px);
  height: calc(50px - 4px);
  line-height: calc(50px - 4px);
  background: #ccc;
  color: #fff;
  text-align: center;
  cursor: ${({ fill }) => fill ? 'not-allowed': 'pointer'};
`

export default TTTCell
