const toggleLog = () => ({
  type: 'TOGGLE_LOG'
})

const receiveLog = data => ({
  type: 'RECEIVE_LOG',
  data
})

const saveLog = action => dispatch => {
  return fetch('http://localhost:3001', {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(action)
  })
    .then(res => {
      dispatch(fetchLog())
    })
}

export const fetchLog = initial => dispatch => {
  return fetch('http://localhost:3001')
    .then(res => res.json())
    .then(res => {
      if (initial) {
        // Turn logging off for initial actions
        dispatch(toggleLog())

        res.map(item => dispatch({ ...item }))

        // Turn logging on again
        dispatch(toggleLog())
      }

      dispatch(receiveLog(res))
    })
}

export const loggingMiddleware = store => next => action => {
  if (
    store.getState().log.log &&
    action.type && !['TOGGLE_LOG', 'RECEIVE_LOG'].includes(action.type)
  ) {
    next(saveLog(action))
  }

  return next(action)
}
