export const restart = () => ({
  type: 'RESTART'
})

export const fill = index => ({
  type: 'FILL',
  index
})
