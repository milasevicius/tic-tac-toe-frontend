How to start project?

* `docker build -t tic-tac-toe-frontend .`
* `docker run -it -v ${PWD}:/usr/src/app -v /usr/src/app/node_modules -p 3000:3000 --rm tic-tac-toe-frontend`
